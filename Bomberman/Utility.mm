//
//  Utility.m
//  Bomberman
//
//  Created by Sergey Zhitnikov on 28.05.14.
//  Copyright (c) 2014 Serzh. All rights reserved.
//
#import "IUtility.hpp"

using namespace std;

class Utility : public IUtility {
public:
    string GetResourcePath() const
    {
        NSString* bundlePath =[[NSBundle mainBundle] resourcePath];
        return [bundlePath UTF8String];
    }
    void LoadPngImage(const string& name)
    {
        NSString* basePath = [[NSString alloc] initWithUTF8String:name.c_str()];
        NSBundle* mainBundle = [NSBundle mainBundle];
        NSString* fullPath = [mainBundle pathForResource:basePath ofType:@"png"];
        UIImage* uiImage = [[UIImage alloc] initWithContentsOfFile:fullPath];
        CGImageRef cgImage = uiImage.CGImage;
        m_imageSize.x = CGImageGetWidth(cgImage);
        m_imageSize.y = CGImageGetHeight(cgImage);
        m_imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    }
    void* GetImageData()
    {
        return (void*) CFDataGetBytePtr(m_imageData);
    }
    ivec2 GetImageSize()
    {
        return m_imageSize;
    }
    void UnloadImage()
    {
        CFRelease(m_imageData);
    }
    void ReadMap(int row, int col, MapObject* lmap[col], const char* cname)
    {
        NSError* error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithUTF8String:cname] ofType:@"map"];
        
        NSString *content = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        
        NSLog(@"%@", content);
        
        MapObject mo;
        const char* cfile = [[content stringByReplacingOccurrencesOfString:@"\n" withString:@""] fileSystemRepresentation];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                switch (*cfile++) {
                    case '0':
                        mo = Empty;
                        break;
                        
                    case '1':
                        mo = Block;
                        break;
                        
                    case '3':
                        mo = Enemy;
                        break;
                        
                    case '6':
                        mo = Player;
                        break;
                        
                    default:
                        mo = Empty;
                        break;
                }
                lmap[i][j] = mo;
            }
        }
    }

private:
    ivec2 m_imageSize;
    CFDataRef m_imageData;
};

IUtility* CreateUtility()
{
    return new Utility();
}
