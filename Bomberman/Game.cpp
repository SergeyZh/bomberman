//
//  Game.cpp
//  Bomberman
//
//  Created by Sergey Zhitnikov on 28.05.14.
//  Copyright (c) 2014 Serzh. All rights reserved.
//

#include "Game.hpp"

#define STRINGIFY(A) #A
#include "Shaders/TexturedLighting.es2.vert"
#include "Shaders/TexturedLighting.es2.frag"

const vec3 axis(0, 1, 0); //Base axis

static class Player player = Player::GetInstance();
std::vector<GameObject> scene;
std::vector<class Enemy> enemies;
std::vector<Bomb> bombs;
std::vector<class Explosion> explosions;
Plane screens[3];

static vec4 lightPosition(0.25, 0.5, 1, 0); //Scene light source
static ivec2 prev;

Game::Game(IUtility *utility)
{
    // Create & bind the color buffer so that the caller can allocate its space.
    m_resourceManager = utility;
    glGenRenderbuffers(1, &m_colorRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, m_colorRenderbuffer);
}

void Game::Initialize(int v_width, int v_height)
{
    // Create the depth buffer.
    glGenRenderbuffers(1, &m_depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, m_depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER,
                          GL_DEPTH_COMPONENT16,
                          v_width,
                          v_height);
    
    // Create the framebuffer object; attach the depth and color buffers.
    glGenFramebuffers(1, &m_framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                              GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER,
                              m_colorRenderbuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                              GL_DEPTH_ATTACHMENT,
                              GL_RENDERBUFFER,
                              m_depthRenderbuffer);
    
    // Bind the color buffer for rendering.
    glBindRenderbuffer(GL_RENDERBUFFER, m_colorRenderbuffer);
    
    // Set up some GL state.
    glViewport(0, 0, v_width, v_height);
    
    glGenBuffers(1, &m_bl_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_bl_vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
    
    glGenBuffers(1, &m_bl_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bl_indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeIndices), cubeIndices, GL_STATIC_DRAW);
    
    glGenBuffers(1, &m_ps_vertexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ps_vertexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(personVertices), personVertices, GL_STATIC_DRAW);
    
    glGenBuffers(1, &m_ps_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ps_indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeIndices), cubeIndices, GL_STATIC_DRAW);
    
    glGenBuffers(1, &m_fl_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_fl_vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(floorVertices), floorVertices, GL_STATIC_DRAW);
    
    glGenBuffers(1, &m_fl_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_fl_indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(floorIndices), floorIndices, GL_STATIC_DRAW);
    
    glGenBuffers(1, &m_boom_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_boom_vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(explosionVertices), explosionVertices, GL_STATIC_DRAW);
    
    glGenBuffers(1, &m_boom_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_boom_indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(explosionIndices), explosionIndices, GL_STATIC_DRAW);
    
    // Build the GLSL program.
    m_textureProgram = BuildProgram(SimpleVertexShader, SimpleFragmentShader);
    glUseProgram(m_textureProgram);
    
    // Extract the handles to attributes and uniforms.
    m_attributes.Position = glGetAttribLocation(m_textureProgram, "Position");
    m_attributes.Normal = glGetAttribLocation(m_textureProgram, "Normal");
    m_attributes.DiffuseMaterial = glGetAttribLocation(m_textureProgram, "DiffuseMaterial");
    m_attributes.TextureCoord = glGetAttribLocation(m_textureProgram, "TextureCoord");
    m_uniforms.Projection = glGetUniformLocation(m_textureProgram, "Projection");
    m_uniforms.Modelview = glGetUniformLocation(m_textureProgram, "Modelview");
    m_uniforms.NormalMatrix = glGetUniformLocation(m_textureProgram, "NormalMatrix");
    m_uniforms.LightPosition = glGetUniformLocation(m_textureProgram, "LightPosition");
    m_uniforms.AmbientMaterial = glGetUniformLocation(m_textureProgram, "AmbientMaterial");
    m_uniforms.SpecularMaterial = glGetUniformLocation(m_textureProgram, "SpecularMaterial");
    m_uniforms.Shininess = glGetUniformLocation(m_textureProgram, "Shininess");
    m_uniforms.Sampler = glGetUniformLocation(m_textureProgram, "Sampler");
    
    // Set the projection matrix.
    GLint projectionUniform = glGetUniformLocation(m_textureProgram, "Projection");
    mat4 projectionMatrix = mat4::Perspective(45, 0.75, 1, 15);
    glUniformMatrix4fv(projectionUniform, 1, 0, projectionMatrix.Pointer());

    // Set the active sampler to stage 0.  Not really necessary since the uniform
    // defaults to zero anyway, but good practice.
	glActiveTexture(GL_TEXTURE0);
    glUniform1i(m_uniforms.Sampler, 0);

    // Load textures.
	SetupTexture(&m_gridTexture, "tile_floor");
    SetupTexture(&m_decTexture, "stonefloor");
    SetupTexture(&m_playerTexture, "Bomberman-icon");
    SetupTexture(&m_bombTexture, "Bomb");
    SetupTexture(&m_boomTexture, "Boom");
    SetupTexture(&m_monsterTexture, "Monster");
    
    SetupTexture(&m_screen[0], "StartScreen");
    SetupTexture(&m_screen[1], "GameOver");
    SetupTexture(&m_screen[2], "WinScreen");
    

    // Initialize various state.
    glEnableVertexAttribArray(m_attributes.Position);
    glEnableVertexAttribArray(m_attributes.Normal);
    glEnableVertexAttribArray(m_attributes.TextureCoord);
    glEnable(GL_DEPTH_TEST);
    
    //Load level
    for (int i = 0;  i < ROW; i++) {
        level[i] = new MapObject[COL];
    }

    SetupScene();
}

void Game::SetupScene()
{
    m_resourceManager->ReadMap(ROW, COL, level, "level");
    
    scene.clear();
    enemies.clear();
    bombs.clear();
    explosions.clear();
    
    Plane startScreen(vec2(ROW/2, COL/2));
    startScreen.rotation = mat4::Rotate(0, axis);
    startScreen.color = vec4(0.75, 0.75, 0.75, 1);
    startScreen.texture = m_screen[0];
    startScreen.vertexBuffer = m_fl_vertexBuffer;
    startScreen.indicesBuffer = m_fl_indexBuffer;
    screens[0] = startScreen;
    
    Plane overScreen(vec2(ROW/2, COL/2));
    overScreen.rotation = mat4::Rotate(0, axis);
    overScreen.color = vec4(0.75, 0.75, 0.75, 1);
    overScreen.texture = m_screen[1];
    overScreen.vertexBuffer = m_fl_vertexBuffer;
    overScreen.indicesBuffer = m_fl_indexBuffer;
    screens[1] = overScreen;
    
    Plane winScreen(vec2(ROW/2, COL/2));
    winScreen.rotation = mat4::Rotate(0, axis);
    winScreen.color = vec4(0.75, 0.75, 0.75, 1);
    winScreen.texture = m_screen[2];
    winScreen.vertexBuffer = m_fl_vertexBuffer;
    winScreen.indicesBuffer = m_fl_indexBuffer;
    screens[2] = winScreen;
    
    Plane floor(vec2(ROW/2, COL/2));
    floor.rotation = mat4::Rotate(135, axis);
    floor.color = vec4(0.75, 0.75, 0.75, 1);
    floor.texture = m_decTexture;
    floor.vertexBuffer = m_fl_vertexBuffer;
    floor.indicesBuffer = m_fl_indexBuffer;
    scene.push_back(floor);
    
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            switch (level[i][j]) {
                case Block:
                {
                    class Block block(vec2(i,j));
                    block.rotation = mat4::Rotate(45, axis);
                    block.color = vec4(0.75, 0.75, 0.75, 1);
                    block.texture = m_gridTexture;
                    block.vertexBuffer = m_bl_vertexBuffer;
                    block.indicesBuffer = m_bl_indexBuffer;
                    scene.push_back(block);
                    break;
                }
                    
                case Player:
                {
                    player.UpdateCoordinate(vec2(i,j));
                    player.rotation = mat4::Rotate(75, axis);
                    player.color = vec4(0.75, 0.75, 0.75, 1);
                    player.texture = m_playerTexture;
                    player.vertexBuffer = m_ps_vertexBuffer;
                    player.indicesBuffer = m_ps_indexBuffer;
                    player.direction = vec2(0,0);
                    player.velocity = 0.3f;
                    break;
                }
                    
                case Enemy:
                {
                    class Enemy enemy(vec2(i,j));
                    enemy.rotation = mat4::Rotate(75, axis);
                    enemy.color = vec4(0.75, 0.75, 0.75, 1);
                    enemy.texture = m_monsterTexture;
                    enemy.vertexBuffer = m_ps_vertexBuffer;
                    enemy.indicesBuffer = m_ps_indexBuffer;
                    enemy.direction = vec2(0,-1);
                    enemy.velocity = 0.3f;
                    enemies.push_back(enemy);
                    break;
                }
                    
                default:
                    break;
            }
        }
    }
    
    m_winCondtition = enemies.size();
}

void Game::SetPreferedGameState(GameState pstate)
{
    if (pstate == Active) {
        if (m_gameState == Win || m_gameState == Lose) SetupScene(); //Restart
        
        m_gameState = Active;
        return;
    }
    
    if (pstate == Pause) {
        if (m_gameState == Active || m_gameState == Pause) {m_gameState = Pause; return;}
        if (m_gameState == Lose) {m_gameState = Lose; return;}
        if (m_gameState == Win) {m_gameState = Win; return;}
    }
    
    else m_gameState = pstate;
}

#pragma mark -
#pragma mark Rendering

void Game::Render() const
{
    //Enable alpha chanel
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    
    //Refresh
    glClearColor(0.5f, 0.5f, 0.5f, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
 
    //Static scene drawing
    for (std::vector<GameObject>::iterator it = scene.begin(); it != scene.end(); ++it) {
        Draw(it->translation, it->rotation, it->color,
             lightPosition, it->texture, it->vertexBuffer,
             it->indicesBuffer, it->GetIndices(), it->GetIndicesSize());
    }
    
    //Dynamic objects drawing
    for (std::vector<class Enemy>::iterator it = enemies.begin(); it != enemies.end(); ++it) {
        Draw(it->translation, it->rotation, it->color,
             lightPosition, it->texture, it->vertexBuffer,
             it->indicesBuffer, it->GetIndices(), it->GetIndicesSize());
    }
    
    Draw(player.translation, player.rotation, player.color, lightPosition, player.texture, player.vertexBuffer, player.indicesBuffer, player.GetIndices(), player.GetIndicesSize());
    
    for (std::vector<Bomb>::iterator it = bombs.begin(); it != bombs.end(); ++it) {
        Draw(it->translation, it->rotation, it->color,
             lightPosition, it->texture, it->vertexBuffer,
             it->indicesBuffer, it->GetIndices(), it->GetIndicesSize());
    }
    
    for (std::vector<class Explosion>::iterator it = explosions.begin(); it != explosions.end(); ++it) {
        Draw(it->translation, it->rotation, it->color,
             lightPosition, it->texture, it->vertexBuffer,
             it->indicesBuffer, it->GetIndices(), it->GetIndicesSize());
    }
    
    //Sign drawing
    switch (m_gameState) {
        case Start:
        case Pause:
            Draw(screens[0].translation, screens[0].rotation, screens[0].color, lightPosition, screens[0].texture, screens[0].vertexBuffer, screens[0].indicesBuffer, screens[0].GetIndices(), screens[0].GetIndicesSize());
            break;
            
        case Lose:
            Draw(screens[1].translation, screens[1].rotation, screens[1].color, lightPosition, screens[1].texture, screens[1].vertexBuffer, screens[1].indicesBuffer, screens[1].GetIndices(), screens[1].GetIndicesSize());
            break;
            
        case Win:
            Draw(screens[2].translation, screens[2].rotation, screens[2].color, lightPosition, screens[2].texture, screens[2].vertexBuffer, screens[2].indicesBuffer, screens[2].GetIndices(), screens[2].GetIndicesSize());
            break;
            
        default:
            break;
    }
}

void Game::Draw(mat4 translation, mat4 rotation, vec4 color, vec4 lightPosition, GLuint texture, GLuint vertexBuffer, GLuint indeciesBuffer, const GLubyte *indices, GLsizei indicesSize) const
{
    //Set Modelview matrix
    mat4 modelview = rotation * translation;
    glUniformMatrix4fv(m_uniforms.Modelview, 1, 0, modelview.Pointer());
    
    // Set the light position.
    glUniform3fv(m_uniforms.LightPosition, 1, lightPosition.Pointer());
    
    // Set the normal matrix.
    mat3 normalMatrix = modelview.ToMat3();
    glUniformMatrix3fv(m_uniforms.NormalMatrix, 1, 0, normalMatrix.Pointer());
    
    // Set the diffuse color.
    glVertexAttrib3fv(m_attributes.DiffuseMaterial, color.Pointer());
    
    // Set up some default material parameters.
    glUniform3f(m_uniforms.AmbientMaterial, 0.04f, 0.04f, 0.04f);
    glUniform3f(m_uniforms.SpecularMaterial, 0.5, 0.5, 0.5);
    glUniform1f(m_uniforms.Shininess, 50);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(texture, 0);
    
    const GLvoid* normalOffset = (const GLvoid*) sizeof(vec3);
    const GLvoid* texCoordOffset = (const GLvoid*) (2 * sizeof(vec3));
    GLint position = m_attributes.Position;
    GLint normal = m_attributes.Normal;
    GLint texCoord = m_attributes.TextureCoord;
    
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indeciesBuffer);
    
    glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
    glVertexAttribPointer(normal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), normalOffset);
    glVertexAttribPointer(texCoord, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), texCoordOffset);
    
    glDrawElements(GL_TRIANGLES, indicesSize, GL_UNSIGNED_BYTE, 0);
}

void Game::Update(float timeStep)
{
    //Win state
    if (m_winCondtition == 0) {
        SetPreferedGameState(Win);
    }
    
    if (m_gameState == Active) {
        //Map intersection detection
        if (level[Round::round_half_up(player.GetCoordinate().x)][Round::round_half_up(player.GetCoordinate().y)] == Enemy ||
            level[Round::round_half_up(player.GetCoordinate().x)][Round::round_half_up(player.GetCoordinate().y)] == Explosion)
        {
            SetPreferedGameState(Lose);
        }
        
        //Player movement
        player.step.x = -player.direction.x * TILE * player.velocity;
        player.step.y =  player.direction.y * TILE * player.velocity;
        
        if (CanMoveTo(player, player.direction)) {
            player.UpdateCoordinate(player.GetCoordinate() + player.step);
            
            vec2 oldPosition = player.GetCoordinate();
            vec2 newPosition = player.GetCoordinate() + player.step;
            
            if (level[Round::round_half_up(newPosition.x)][Round::round_half_up(newPosition.y)] != Block) {
                level[Round::round_half_up(oldPosition.x)][Round::round_half_up(oldPosition.y)] = Empty;
                level[Round::round_half_up(newPosition.x)][Round::round_half_up(newPosition.y)] = Player;
            }
            else {
                player.UpdateCoordinate(player.GetCoordinate() - player.step);
            }
        }
        
        else {
            player.direction = vec2(0,0);
        }
        
        //Enemies movement
        for (std::vector<class Enemy>::iterator it = enemies.begin(); it != enemies.end(); ++it) {
            if (it->IsActive()) {
                if (CanMoveTo(*it, it->direction)) {
                    it->step.x = -it->direction.x * TILE * it->velocity;
                    it->step.y =  it->direction.y * TILE * it->velocity;
                    
                    it->UpdateCoordinate(it->GetCoordinate() + it->step);
                    
                    vec2 oldPosition = it->GetCoordinate();
                    vec2 newPosition = it->GetCoordinate() + it->step;
                    
                    if (level[Round::round_half_up(newPosition.x)][Round::round_half_up(newPosition.y)] == Explosion) {
                        it->Kill();
                        m_winCondtition--;
                    }
                    
                    if (level[Round::round_half_up(newPosition.x)][Round::round_half_up(newPosition.y)] != Block) {
                        level[Round::round_half_up(oldPosition.x)][Round::round_half_up(oldPosition.y)] = Empty;
                        level[Round::round_half_up(newPosition.x)][Round::round_half_up(newPosition.y)] = Enemy;
                    }
                }
                
                //Simple patrolling
                else {
                    it->direction.x = -it->direction.x;
                    it->direction.y = -it->direction.y;
                }
            }
        }
        
        for (std::vector<Bomb>::iterator it = bombs.begin(); it != bombs.end(); ++it) {
            if (bombs.empty()) {
                break;
            }
            
            //Chain reactioon
            if (level[Round::round_half_up(it->GetCoordinate().x)][Round::round_half_up(it->GetCoordinate().y)] == Explosion)
            {
                GenExplosion(*it);
                std::swap(bombs.front(), bombs.back());
                bombs.pop_back();
            }
            
            //Ticker
            it->timer -= timeStep;
            if (it->timer <= 2.0) {it->rotation = mat4::Rotate(60, axis);}
            if (it->timer <= 1.0) {it->rotation = mat4::Rotate(75, axis);}
            if (it->timer <= 0.0f) {
                GenExplosion(*it);
                std::swap(bombs.front(), bombs.back());
                bombs.pop_back();
            };
        }
        
        for (std::vector<class Explosion>::iterator it = explosions.begin(); it != explosions.end(); ++it) {
            if (explosions.empty()) {
                //Forced cleanup
                for (int i = 0; i < ROW; i++) {
                    for (int j = 0; j < COL; j++) {
                        if (level[i][j] == Explosion)
                        {
                            level[i][j] = Empty;
                        }
                    }
                }
                break;
            }
            
            it->lifetime -= timeStep;
            if (it->lifetime <= 0.0f) {
                level[(int)(it->GetCoordinate().x)][(int)(it->GetCoordinate().y)] = Empty;
                std::swap(explosions.front(), explosions.back());
                explosions.pop_back();
            };
        }
        
    }
}

#pragma mark -
#pragma mark Shader Compilation

GLuint Game::BuildShader(const char* source, GLenum shaderType) const
{
    GLuint shaderHandle = glCreateShader(shaderType);
    glShaderSource(shaderHandle, 1, &source, 0);
    glCompileShader(shaderHandle);
    
    GLint compileSuccess;
    glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSuccess);
    
    if (compileSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetShaderInfoLog(shaderHandle, sizeof(messages), 0, &messages[0]);
        std::cout << messages;
        exit(1);
    }
    
    return shaderHandle;
}

GLuint Game::BuildProgram(const char* vertexShaderSource,
                                      const char* fragmentShaderSource) const
{
    GLuint vertexShader = BuildShader(vertexShaderSource, GL_VERTEX_SHADER);
    GLuint fragmentShader = BuildShader(fragmentShaderSource, GL_FRAGMENT_SHADER);
    
    GLuint programHandle = glCreateProgram();
    glAttachShader(programHandle, vertexShader);
    glAttachShader(programHandle, fragmentShader);
    glLinkProgram(programHandle);
    
    GLint linkSuccess;
    glGetProgramiv(programHandle, GL_LINK_STATUS, &linkSuccess);
    if (linkSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetProgramInfoLog(programHandle, sizeof(messages), 0, &messages[0]);
        std::cout << messages;
        exit(1);
    }
    
    return programHandle;
}

#pragma mark -
void Game::SetupTexture(GLuint* texName, const std::string sname)
{
    glGenTextures(1, texName);
    glBindTexture(GL_TEXTURE_2D, *texName);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    m_resourceManager->LoadPngImage(sname);
    void* pixels = m_resourceManager->GetImageData();
    ivec2 size = m_resourceManager->GetImageSize();
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    m_resourceManager->UnloadImage();
    glGenerateMipmap(GL_TEXTURE_2D);
}

#pragma mark -
#pragma mark Input

void Game::OnFingerUp(ivec2 location)
{
    if (m_shouldPlace) {
        PlaceBomb(vec2(Round::round_half_up(player.GetCoordinate().x), Round::round_half_up(player.GetCoordinate().y)));
    }
    
    if (m_shouldTrace) {
        player.direction = vec2(location - prev).Normalized();
        player.direction.x = Round::round_half_up(player.direction.x);
        player.direction.y = Round::round_half_up(player.direction.y);
    }
}

void Game::OnFingerDown(ivec2 location)
{
    if (m_gameState != Active) {
        SetPreferedGameState(Active);
        m_shouldPlace = false;
        m_shouldTrace = false;
        return; //Avoid bomb placing
    }
    
    m_shouldPlace = true;
    m_shouldTrace = false;
}

void Game::OnFingerMove(ivec2 previous, ivec2 location)
{
    m_shouldPlace = false;
    m_shouldTrace = true;
    prev = previous;
}

#pragma mark -
#pragma mark Logic calculation

bool Game::CanMoveTo(GameObject go, vec2 dir)
{
    vec2 newDir = go.GetCoordinate() + vec2(dir.x * -1, dir.y);
    
    if (newDir.x < 0 || newDir.x > ROW - 1 || newDir.y < 0 || newDir.y > COL - 1) {return false;}
    
    //AABB Collision detection
    for (std::vector<GameObject>::iterator block = scene.begin() + 1; block != scene.end(); ++block) {
        if (fabs(newDir.x - block->GetCoordinate().x) < TILE) {
            if (fabs(newDir.y - block->GetCoordinate().y) < TILE) {
                return false;
            }
        }
    }
    
    return true;
}

void Game::PlaceBomb(vec2 coord)
{
    if (bombs.size() < MAX_BOMB_COUNT) {
        Bomb bomb(coord);
        bomb.rotation = mat4::Rotate(45, axis);
        bomb.color = vec4(0.75, 0.75, 0.75, 1);
        bomb.texture = m_bombTexture;
        bomb.vertexBuffer = m_ps_vertexBuffer;
        bomb.indicesBuffer = m_ps_indexBuffer;
        bomb.timer = 3.0f;
        bomb.explosionForce = 8;
        bombs.push_back(bomb);
    }
}

void Game::GenExplosion(Bomb bomb)
{
    PlaceBoom(bomb.GetCoordinate() - vec2(0.5f,0));
    
    bool up = true, right = true, left = true, down = true;
    
    for (int n = 1; bomb.explosionForce > 0; n++) {
        //Borders validation
        if (Round::round_half_up(bomb.GetCoordinate().x) - 0.5f - n < 0) up = false;
        if (Round::round_half_up(bomb.GetCoordinate().x) - 0.5f + n > ROW - 1) down = false;
        if (Round::round_half_up(bomb.GetCoordinate().y) - n < 0) left = false;
        if (Round::round_half_up(bomb.GetCoordinate().y) + n > COL - 1) right = false;
        
        //If one direction is blocked then explosion power go to another
        if (up && level[Round::round_half_up(bomb.GetCoordinate().x) - n][Round::round_half_up(bomb.GetCoordinate().y)] != Block) { PlaceBoom(bomb.GetCoordinate() - vec2(0.5f + n, 0)); bomb.explosionForce--;}
        else up = false;
        
        if (right && level[Round::round_half_up(bomb.GetCoordinate().x)][Round::round_half_up(bomb.GetCoordinate().y) + n] != Block) { PlaceBoom(bomb.GetCoordinate() - vec2(0.5f, -n)); bomb.explosionForce--;}
        else right = false;
        
        if (down && level[Round::round_half_up(bomb.GetCoordinate().x) + n][Round::round_half_up(bomb.GetCoordinate().y)] != Block) { PlaceBoom(bomb.GetCoordinate() - vec2(0.5f - n, 0)); bomb.explosionForce--;}
        else down = false;
        
        if (left && level[Round::round_half_up(bomb.GetCoordinate().x)][Round::round_half_up(bomb.GetCoordinate().y) - n] != Block) { PlaceBoom(bomb.GetCoordinate() - vec2(0.5f, n)); bomb.explosionForce--;}
        else left = false;
        
        if (!up && !right && !left && !down) {
            break;
        }
    }
}

void Game::PlaceBoom(vec2 coord)
{
    class Explosion explosion(coord, 0.5f);
    explosion.rotation = mat4::Rotate(135, axis);
    explosion.color = vec4(1, 1, 1, 0.5);
    explosion.texture = m_boomTexture;
    explosion.vertexBuffer = m_boom_vertexBuffer;
    explosion.indicesBuffer = m_boom_indexBuffer;
    explosions.push_back(explosion);
    level[Round::round_half_up(coord.x)][Round::round_half_up(coord.y)] = Explosion;
}


