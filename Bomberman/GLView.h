//
//  GLView.h
//  Bomberman
//
//  Created by Sergey Zhitnikov on 28.05.14.
//  Copyright (c) 2014 Serzh. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "IUtility.hpp"
#include "Game.hpp"

@interface GLView : UIView {
    EAGLContext* m_context;
    Game* m_gameEngine;
    IUtility* m_resourceManager;
    float m_timestamp;
}

- (void)drawView:(CADisplayLink*)displayLink;
- (void)setState:(GameState)state;
//- (void)didRotate:(NSNotification*)notification;

@end
