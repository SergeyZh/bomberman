//
//  AppDelegate.h
//  Bomberman
//
//  Created by Sergey Zhitnikov on 28.05.14.
//  Copyright (c) 2014 Serzh. All rights reserved.
//

#import "GLView.h"
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) GLView* glView;

@end
