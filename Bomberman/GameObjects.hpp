//
//  GameObjects.hpp
//  Bomberman
//
//  Created by Sergey Zhitnikov on 02.06.14.
//  Copyright (c) 2014 Serzh. All rights reserved.
//

#ifndef Bomberman_GameObjects_hpp
#define Bomberman_GameObjects_hpp

#include "Vector.hpp"
#include "Matrix.hpp"
#include "Quaternion.hpp"
#include "Round.hpp"
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#include <vector>

#define TEX_COORD_MAX 1
#define HALF_TILE 0.1
#define TILE HALF_TILE*2
#define COL 17
#define ROW 11
#define SCALE 1.5

#define ORX 1.4
#define ORY 2.4
#define ORZ -10

#define MAX_BOMB_COUNT 2

//Map specification
enum MapObject {
    Empty = 0,
    Block = 1,
    Enemy = 3,
    Player= 6,
    Explosion = 8
};

//Map array
static MapObject *level[COL];

typedef struct {
    float Position[3];
    float Normal[3];
    float TexCoord[2];
} Vertex;

#pragma mark Primitives

//Hardcoded primitives

//Big plane
const Vertex floorVertices[] = {
    {{3, -3, 0.2}, {0, 1, 0}, {0, 0}},
    {{3, 3, 0.2}, {0, 1, 0}, {0, TEX_COORD_MAX}},
    {{-3, 3, 0.2}, {0, 1, 0}, {TEX_COORD_MAX, TEX_COORD_MAX}},
    {{-3, -3, 0.2}, {0, 1, 0}, {TEX_COORD_MAX, 0}}
};

const GLubyte floorIndices[] = {
    0, 1, 2,
    2, 3, 0
};

//Small plane
const Vertex explosionVertices[] = {
    {{HALF_TILE, -HALF_TILE, HALF_TILE}, {0, 1, 0}, {1, 0}},
    {{HALF_TILE, HALF_TILE, HALF_TILE}, {0, 1, 0}, {1, 1}},
    {{-HALF_TILE, HALF_TILE, HALF_TILE}, {0, 1, 0}, {0, 1}},
    {{-HALF_TILE, -HALF_TILE, HALF_TILE}, {0, 1, 0}, {0, 0}}
};

const GLubyte explosionIndices[] = {
    0, 1, 2,
    2, 3, 0
};

//Cube
const Vertex cubeVertices[] = {
    // Front
    {{HALF_TILE, -HALF_TILE, 0}, {0, 0, 1}, {1, 0}},
    {{HALF_TILE, HALF_TILE, 0}, {0, 0, 1}, {1, 1}},
    {{-HALF_TILE, HALF_TILE, 0}, {0, 0, 1}, {0, 1}},
    {{-HALF_TILE, -HALF_TILE, 0}, {0, 0, 1}, {0, 0}},
    // Back
    {{HALF_TILE, HALF_TILE, -TILE}, {0, 0, -1}, {1, 0}},
    {{-HALF_TILE, -HALF_TILE, -TILE}, {0, 0, -1}, {1, 1}},
    {{HALF_TILE, -HALF_TILE, -TILE}, {0, 0, -1}, {0, 1}},
    {{-HALF_TILE, HALF_TILE, -TILE}, {0, 0, -1}, {0, 0}},
    // Left
    {{-HALF_TILE, -HALF_TILE, 0}, {-1, 0, 0}, {1, 1}},
    {{-HALF_TILE, HALF_TILE, 0}, {-1, 0, 0}, {1, 1}},
    {{-HALF_TILE, HALF_TILE, -TILE}, {-1, 0, 0}, {0, 1}},
    {{-HALF_TILE, -HALF_TILE, -TILE}, {-1, 0, 0}, {0, 0}},
    // Right
    {{HALF_TILE, -HALF_TILE, -TILE}, {1, 0, 0}, {1, 0}},
    {{HALF_TILE, HALF_TILE, -TILE}, {1, 0, 0}, {1, 1}},
    {{HALF_TILE, HALF_TILE, 0}, {1, 0, 0}, {0, 1}},
    {{HALF_TILE, -HALF_TILE, 0}, {1, 0, 0}, {0, 0}},
    // Top
    {{HALF_TILE, HALF_TILE, 0}, {0, 1, 0}, {1, 0}},
    {{HALF_TILE, HALF_TILE, -TILE}, {0, 1, 0}, {1, 1}},
    {{-HALF_TILE, HALF_TILE, -TILE}, {0, 1, 0}, {0, 1}},
    {{-HALF_TILE, HALF_TILE, 0}, {0, 1, 0}, {0, 0}},
    // Bottom
    {{HALF_TILE, -HALF_TILE, -TILE}, {0, -1, 0}, {1, 0}},
    {{HALF_TILE, -HALF_TILE, 0}, {0, -1, 0}, {1, 1}},
    {{-HALF_TILE, -HALF_TILE, 0}, {0, -1, 0}, {0, 1}},
    {{-HALF_TILE, -HALF_TILE, -TILE}, {0, -1, 0}, {0, 0}}
};

//Cube with different texture mapping
const Vertex personVertices[] = {
    // Front
    {{HALF_TILE, -HALF_TILE, 0}, {0, 0, 1}, {0, 0}},
    {{HALF_TILE, HALF_TILE, 0}, {0, 0, 1}, {0, 0}},
    {{-HALF_TILE, HALF_TILE, 0}, {0, 0, 1}, {0, 0}},
    {{-HALF_TILE, -HALF_TILE, 0}, {0, 0, 1}, {0, 0}},
    // Back
    {{HALF_TILE, HALF_TILE, -TILE}, {0, 0, -1}, {0, 0}},
    {{-HALF_TILE, -HALF_TILE, -TILE}, {0, 0, -1}, {0, 0}},
    {{HALF_TILE, -HALF_TILE, -TILE}, {0, 0, -1}, {0, 0}},
    {{-HALF_TILE, HALF_TILE, -TILE}, {0, 0, -1}, {0, 0}},
    // Left
    {{-HALF_TILE, -HALF_TILE, 0}, {-1, 0, 0}, {0, 0}},
    {{-HALF_TILE, HALF_TILE, 0}, {-1, 0, 0}, {0, 0}},
    {{-HALF_TILE, HALF_TILE, -TILE}, {-1, 0, 0}, {0, 0}},
    {{-HALF_TILE, -HALF_TILE, -TILE}, {-1, 0, 0}, {0, 0}},
    // Right
    {{HALF_TILE, -HALF_TILE, -TILE}, {1, 0, 0}, {0, 0}},
    {{HALF_TILE, HALF_TILE, -TILE}, {1, 0, 0}, {1, 0}},
    {{HALF_TILE, HALF_TILE, 0}, {1, 0, 0}, {1, 1}},
    {{HALF_TILE, -HALF_TILE, 0}, {1, 0, 0}, {0, 1}},
    // Top
    {{HALF_TILE, HALF_TILE, 0}, {0, 1, 0}, {0, 0}},
    {{HALF_TILE, HALF_TILE, -TILE}, {0, 1, 0}, {0, 0}},
    {{-HALF_TILE, HALF_TILE, -TILE}, {0, 1, 0}, {0, 0}},
    {{-HALF_TILE, HALF_TILE, 0}, {0, 1, 0}, {0, 0}},
    // Bottom
    {{HALF_TILE, -HALF_TILE, -TILE}, {0, -1, 0}, {0, 0}},
    {{HALF_TILE, -HALF_TILE, 0}, {0, -1, 0}, {0, 0}},
    {{-HALF_TILE, -HALF_TILE, 0}, {0, -1, 0}, {0, 0}},
    {{-HALF_TILE, -HALF_TILE, -TILE}, {0, -1, 0}, {0, 0}}
};

const GLubyte cubeIndices[] = {
    // Front
    0, 1, 2,
    2, 3, 0,
    // Back
    4, 5, 6,
    6, 7, 4,
    // Left
    8, 9, 10,
    10, 11, 8,
    // Right
    12, 13, 14,
    14, 15, 12,
    // Top
    16, 17, 18,
    18, 19, 16,
    // Bottom
    20, 21, 22,
    22, 23, 20
};

#pragma mark -

//Root objects class
class GameObject {
protected:
    vec2 coord;

    const Vertex* vertices;
    const GLubyte* indices;
    GLsizei indicesSize;
    
public:
    GameObject() {};
    GameObject(vec2 coord_)
    {
        UpdateCoordinate(coord_);
    }
    ~GameObject() {};
    
    vec2 direction;
    vec2 step = vec2(0,0);
    mat4 translation;
    mat4 rotation;
    vec4 color;
    float velocity;
    GLuint vertexBuffer;
    GLuint indicesBuffer;
    GLuint texture;
    
    void SetVertices(const Vertex vert[])
    {
        vertices = vert;
    }
    
    void SetIndices(const GLubyte inds[], GLsizei size)
    {
        indices = inds;
        indicesSize = size;
    }
    
    const GLubyte* GetIndices()
    {
        return indices;
    }
    
    GLsizei GetIndicesSize()
    {
        return indicesSize;
    }
    
    void UpdateCoordinate(vec2 crntCoord_)
    {
        coord = crntCoord_;
        translation = mat4::Translate(ORX - coord.x * TILE * SCALE, ORY - coord.y * TILE * SCALE, ORZ + coord.x * TILE * SCALE);
    }

    vec2 GetCoordinate()
    {
        return coord;
    }
};

//Wall block
class Block:public GameObject {
public:
    Block(){
        SetVertices(cubeVertices);
        SetIndices(cubeIndices, sizeof(cubeIndices)/sizeof(cubeIndices[0]));
    };
    Block(vec2 coords):GameObject(coords) {
        SetVertices(cubeVertices);
        SetIndices(cubeIndices, sizeof(cubeIndices)/sizeof(cubeIndices[0]));
    };
    ~Block() {};
};

//Floor plane
class Plane:public GameObject {
public:
    Plane() {
        SetVertices(floorVertices);
        SetIndices(floorIndices, sizeof(floorIndices)/sizeof(floorIndices[0]));
    };
    Plane(vec2 coords):GameObject(coords) {
        SetVertices(floorVertices);
        SetIndices(floorIndices, sizeof(floorIndices)/sizeof(floorIndices[0]));
    };
    ~Plane() {};
};

//Player singleton
class Player: public GameObject {
private:
    Player() {
        SetVertices(personVertices);
        SetIndices(cubeIndices, sizeof(cubeIndices)/sizeof(cubeIndices[0]));
    };
    
public:
    static const Player& GetInstance()
    {
        static Player instance;
        
        return instance;
    }
};

class Enemy: public GameObject {
protected:
    bool isActive;
    
public:
    Enemy(vec2 coords):GameObject(coords) {
        SetVertices(personVertices);
        SetIndices(cubeIndices, sizeof(cubeIndices)/sizeof(cubeIndices[0]));
        isActive = true;
    };
    ~Enemy() {};
    
    void Kill() {
        UpdateCoordinate(vec2(-100, -100));
        isActive = false;
    }
    
    bool IsActive() {
        return isActive;
    }
};

class Explosion: public GameObject {
public:
    Explosion() {
        SetVertices(explosionVertices);
        SetIndices(explosionIndices, sizeof(explosionIndices)/sizeof(floorIndices[0]));
    };
    Explosion(vec2 coords, float life):GameObject(coords) {
        SetVertices(explosionVertices);
        SetIndices(explosionIndices, sizeof(explosionIndices)/sizeof(explosionIndices[0]));
        lifetime = life;
    };
    ~Explosion() {};

    float lifetime;
};


class Bomb: public GameObject {
public:
    Bomb(){
        SetVertices(personVertices);
        SetIndices(cubeIndices, sizeof(cubeIndices)/sizeof(cubeIndices[0]));
    };
    Bomb(vec2 coords):GameObject(coords) {
        SetVertices(personVertices);
        SetIndices(cubeIndices, sizeof(cubeIndices)/sizeof(cubeIndices[0]));
    };
    ~Bomb() {};
   
    int explosionForce;
    float timer;
};

#endif
