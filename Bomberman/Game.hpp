//
//  Game.h
//  Bomberman
//
//  Created by Sergey Zhitnikov on 28.05.14.
//  Copyright (c) 2014 Serzh. All rights reserved.
//

#ifndef __Bomberman__Game__
#define __Bomberman__Game__

#include <iostream>
#include "GameObjects.hpp"
#include "IUtility.hpp"

enum GameState {
    Start = -1,
    Pause = 0,
    Active= 1,
    Lose  = 2,
    Win   = 3
};

struct UniformHandles {
    GLuint Modelview;
    GLuint Projection;
    GLuint NormalMatrix;
    GLuint LightPosition;
    GLint AmbientMaterial;
    GLint SpecularMaterial;
    GLint Shininess;
    GLint Sampler;
};

struct AttributeHandles {
    GLint Position;
    GLint Normal;
    GLint DiffuseMaterial;
    GLint TextureCoord;
};

class Game {
private:
    //OpenGL shader compilation
    GLuint BuildShader(const char* source, GLenum shaderType) const;
    GLuint BuildProgram(const char* vShader, const char* fShader) const;
    //Texture binding
    void SetupTexture(GLuint*, const std::string);
    
    GameState m_gameState;
    unsigned long m_winCondtition; //Win condition reach zero -> you win
    
    bool m_shouldPlace = true; //Can place bomb
    bool m_shouldTrace = false;
    
    GLuint m_textureProgram;
    GLuint m_framebuffer;
    GLuint m_colorRenderbuffer;
    GLuint m_depthRenderbuffer;
    
    UniformHandles m_uniforms;
    AttributeHandles m_attributes;
    
    GLuint m_gridTexture;
    GLuint m_decTexture;
    GLuint m_playerTexture;
    GLuint m_monsterTexture;
    GLuint m_bombTexture;
    GLuint m_boomTexture;
    GLuint m_screen[3];
   
    IUtility* m_resourceManager;
    
    GLuint m_bl_vertexBuffer;
    GLuint m_bl_indexBuffer;
    
    GLuint m_ps_vertexBuffer;
    GLuint m_ps_indexBuffer;
    
    GLuint m_fl_vertexBuffer;
    GLuint m_fl_indexBuffer;
    
    GLuint m_boom_vertexBuffer;
    GLuint m_boom_indexBuffer;
    
public:
    Game () {};
    Game (IUtility*);
    ~Game() {};
    
    void Initialize(int, int);
    //Setup current level
    void SetupScene();
    //Render current frame
    void Render() const;
    //Calculate game logic
    void Update(float timeStep);
    //Setup game object on screen
    void Draw(mat4 translation, mat4 rotation, vec4 color, vec4 lightPosition, GLuint texture, GLuint vertexBuffer, GLuint indeciesBuffer, const GLubyte* indices, GLsizei indicesSize) const;
    
    void SetPreferedGameState(GameState);
    
    //Input handling
    void OnFingerUp(ivec2 location);
    void OnFingerDown(ivec2 location);
    void OnFingerMove(ivec2 oldLocation, ivec2 newLocation);
    
    //Placing bomb at character coordinate
    void PlaceBomb(vec2);
    //Setup explosion effect
    void PlaceBoom(vec2);
    //Calculate explosion
    void GenExplosion(Bomb);
    
    //Collision detection
    bool CanMoveTo(GameObject, vec2); 
};


#endif /* defined(__Bomberman__RenderingEngine__) */
