//
//  Round.hpp
//  Bomberman
//
//  Created by Sergey Zhitnikov on 11.06.14.
//  Copyright (c) 2014 Serzh. All rights reserved.
//

#ifndef Bomberman_Round_hpp
#define Bomberman_Round_hpp

class Round {
public:
    static int round_half_up(float n)
    {
        return floorf(n + 0.5f);
    }
    
    static int close_up(float n)
    {
        return floorf(n + 0.8f);
    }
    
    static int round_half_down(float n)
    {
        return floorf(n - 0.5f);
    }

};


#endif
