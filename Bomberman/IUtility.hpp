//
//  IUtility.h
//  Bomberman
//
//  Created by Sergey Zhitnikov on 28.05.14.
//  Copyright (c) 2014 Serzh. All rights reserved.
//

#ifndef Bomberman_IUtility_h
#define Bomberman_IUtility_h

#include <string>
#include <iostream>
#include <fstream>
#include "Vector.hpp"
#include "GameObjects.hpp"

//Interface for calling OS specific function
struct IUtility {
    virtual std::string GetResourcePath() const = 0;
    virtual void LoadPngImage(const std::string& filename) = 0;
    virtual void* GetImageData() = 0;
    virtual ivec2 GetImageSize() = 0;
    virtual void UnloadImage() = 0;
    virtual void ReadMap(int row, int col, MapObject*[col], const char*) = 0;
    virtual ~IUtility() {}
};

IUtility* CreateUtility();

#endif
