Simple Bomberman clone for iOS created on C++ and OpenGL ES 2.0.

This is the first version with only one level and simplest patrolling enemy.

### How do I get set up? ###

Current version uses no additional frameworks. The only thing you need to enjoy the game is to open Bomberman.xcodeproj with Xcode and run the project at target device or simulator.

### How to play? ###

You swipe the screen to move the character. Once started moving, the character stops only if met an obstacle. The direction can be changed while the character is moving.

The bombs are placed by touching the screen. You can place only two bombs at the same time. Bombs can trigger each other with explosion.

If your character touched by enemy or explosion - you lose.
If you manage to kill all enemies at the level - you win.
In any case you can replay current level by touching the screen.

### Who do I talk to? ###

For any question and suggestion please contact me at 
sergeyzhi@mail.ru